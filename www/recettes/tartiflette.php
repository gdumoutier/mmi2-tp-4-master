<a href="<?= $route['recette'] ?>">Revenir à la liste de recettes</a>
<hr>
<div itemscope itemtype=”http://schema.org/Recipe”>
	<img itemprop=”image” src="https://files.meilleurduchef.com/mdc/photo/recette/tartiflette/tartiflette-640.jpg" style="float:right; width:200px;" alt="">

	<h1>Tartiflette</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium animi aperiam architecto, asperiores at
		culpa dolor dolores eaque eius facere facilis fugit in itaque iure iusto laborum modi, molestias natus neque
		omnis placeat, quas quia repellendus rerum tempora tempore tenetur voluptas? Accusamus asperiores et iure maxime
		quam sapiente ut veniam!</p>
	<h2>Ingrédients</h2>
	<ul>
		<li>Lorem ipsum dolor.</li>
		<li>Accusamus blanditiis, distinctio!</li>
		<li>Excepturi explicabo, harum?</li>
		<li>Suscipit tenetur, voluptate?</li>
		<li>Natus, praesentium ullam!</li>
		<li>Eveniet, repellat, soluta.</li>
	</ul>
	<h2>Préparation</h2>
	<ol>
		<li>Lorem ipsum dolor sit amet, consectetur.</li>
		<li>Amet corporis labore magnam magni mollitia.</li>
		<li>Commodi numquam quia quis sequi ullam.</li>
		<li>Dolor dolorem nobis obcaecati omnis voluptas!</li>
		<li>Architecto minus rem repellat saepe voluptas.</li>
		<li>Alias doloremque illum libero quisquam voluptate.</li>
	</ol>
</div>
