<a href="<?= $route['recette'] ?>">Revenir à la liste de recettes</a>
<hr>
<div itemscope itemtype="http://schema.org/Recipe">
	<img itemprop="image" src="https://image.afcdn.com/recipe/20130914/42772_w420h344c1cx175cy182.jpg" style="float:right; width:200px;" alt="">
	<h1 itemprop="name">Pâte à crêpes</h1>
	<p>
		<strong>Temps de Préparation :</strong> <meta itemprop="prepTime" content="PT5M">20 minutes <br>
		<strong>Temps de cuisson :</strong> <meta itemprop="cookTime" content="PT5M">1 hour <br>
		<strong>Notes de mes amis :</strong> <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
			<meta itemprop="ratingCount " content="16">
			<meta itemprop="reviewCount" content="4">
			<meta itemprop="ratingValue" content="4.5">4.5 / 5 !</span><br>
	</p>
	<p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium animi aperiam architecto, asperiores at
		culpa dolor dolores eaque eius facere facilis fugit in itaque iure iusto laborum modi, molestias natus neque
		omnis placeat, quas quia repellendus rerum tempora tempore tenetur voluptas? Accusamus asperiores et iure maxime
		quam sapiente ut veniam!</p>
	<h2>Ingrédients</h2>
	<ul>
		<li itemprop="ingredients">Lorem ipsum dolor.</li>
		<li itemprop="ingredients">Accusamus blanditiis, distinctio!</li>
		<li itemprop="ingredients">Excepturi explicabo, harum?</li>
		<li itemprop="ingredients">Suscipit tenetur, voluptate?</li>
		<li itemprop="ingredients">Natus, praesentium ullam!</li>
		<li itemprop="ingredients">Eveniet, repellat, soluta.</li>
	</ul>
	<h2>Préparation</h2>
	<ol itemprop="recipeInstructions">
		<li>Lorem ipsum dolor sit amet, consectetur.</li>
		<li>Amet corporis labore magnam magni mollitia.</li>
		<li>Commodi numquam quia quis sequi ullam.</li>
		<li>Dolor dolorem nobis obcaecati omnis voluptas!</li>
		<li>Architecto minus rem repellat saepe voluptas.</li>
		<li>Alias doloremque illum libero quisquam voluptate.</li>
	</ol>
</div>