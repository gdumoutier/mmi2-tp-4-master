<?php

function sendMail($mailFrom,$mailTo,$subject,$message,$html = true){
	$headers = "MIME-Version: 1.0\r\n";
	if($html){
		$headers .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
	}else{
		$headers .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
	}
	$headers .= "Content-Transfer-Encoding:8bit\r\n";
	$headers .= "To: $mailTo\r\n";
	$headers .= "From:<$mailFrom>\r\n";
	$headers .= "Reply-To: $mailFrom\r\n";
	$headers .= "\r\n"; // introduire une ligne entre l'entête et le texte

	mail($mailTo,$subject,$message."\r\n",$headers);

}
/*
 * Modifier dans le php.ini :
[mail function]
SMTP = localhost
smtp_port = 1025
*/

?>