//@prepros-prepend imports/lazysizes.min.js
//@prepros-prepend imports/jquery.js
//@prepros-prepend imports/jquery.owl.carousel.js
//@prepros-prepend imports/jquery.lightgallery-all.js
//@prepros-prepend imports/autosize.js
//@prepros-prepend imports/remodal.js

(function($){

	/*=========================================\
		$GALERIE
	\=========================================*/

	var insGallery,
	    gallery,
	    galleryOptions = {
		    thumbnail         : true,
		    animateThumb      : true,
		    showThumbByDefault: false,
		    download          : false,
		    selector          : '.unfiltered a'
	    };

    function init_galerie(){
	    if (insGallery) {
		    insGallery.destroy(true);
	    }
	    gallery = $('.galerie');
	    gallery.lightGallery(galleryOptions);
	    insGallery = gallery.first().data('lightGallery');
	}

	/*=========================================\
		$AUTOGROW
	\=========================================*/

	autosize($('textarea'));

	/*=========================================\
		$SLIDER
	\=========================================*/

	function init_carousel() {
		$('.slider').owlCarousel({
			items: 1
		});
	}

	/*=========================================\
		$NAV AJAX
	\=========================================*/

	//$('a[data-page]').click(function(event){
	//	event.preventDefault();
	//	var page = $(this).attr('data-page');
	//	if(page!="undefined"){
	//		loadPage(page);
	//	}
	//});

	//function loadPage(target){
	//	$('nav a.active').removeClass('active');
	//	$('[data-page="'+target+'"]').addClass('active');
	//	$.get("pages/"+target+".html", function( data ) {
	//		$("#pages").html( data );
	//		init_carousel();
	//		init_galerie();
	//		initFilter();
	//		window.lazySizesConfig.lazyClass = 'lazyload';
	//	});
	//}

	init_carousel();
	init_galerie();
	initFilter();
	window.lazySizesConfig.lazyClass = 'lazyload';

	/*=========================================\
		$INIT LOADING PAGE
	\=========================================*/

	loadPage('accueil');

	/*=========================================\
		$FILTERS
	\=========================================*/

	var filter = 0;
	function initFilter(){
		$('#filters').on('click','button',function(e){
			e.preventDefault();
			filter = $(this).data('filter');
			$('.galerie .filtered').removeClass('filtered');
			$('.galerie .unfiltered').removeClass('unfiltered');
			if(filter != 0){
				$('.galerie > div').filter(function() {
					return $(this).data("filter") != filter;
				}).toggleClass('filtered');
			}
			$('.galerie > div').not('.filtered').addClass('unfiltered');
			init_galerie();
		})
	}


})(jQuery);